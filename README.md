
#Code and datasets for N-terminal enrichment analysis of E coli homomers in Natan et al, "Assembly in the translation milieu imposes evolutionary constraints on homomeric proteins
Description#

This contains datasets and R scripts needed to reproduce the figures in Natan et al "Assembly in the translation milieu imposes evolutionary constraints on homomeric proteins". Specifically, it can be used to calculate the N- vs C-terminal interface enrichment for sets of homomeric structures from E coli protome, as in Figs 2C, and Figs S3. The scripts have been tested on Linux system.
